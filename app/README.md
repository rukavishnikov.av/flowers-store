**Для установки на чистый Ubuntu 17.04 server**
1. Создаем пользователя ra и настраиваем доступ к серверу по shh (добавляем публичный ключ в .ssh/authorized_keys )
2. Ставим пакеты

`sudo apt python3 python-dev python3-pip nginx virtualenv`

`pip3 install uwsgi`

3. Заливаем проект любым способом. Содержимое папки `app` в `/home/ra`. Можно выполнить `fab deploy`
4. Создаем папку `/etc/uwsgi/vassals
5. Линкуем или пересоздаем (если нужна другая конфигурация) uwsgi-конфиг

`sudo ln -s /home/ra/conf/uwsgi_skeleton.ini /etc/uwsgi/vassals/flowers.ini`

6. Копируем конфиг императора

 `sudo cp /home/ra/conf/emperor.ini /etc/uwsgi/emperor.ini`

7. Копируем файл службы `emperor.uwsgi.service`

 `sudo cp /home/ra/conf/emperor.uwsgi.service /etc/systemd/system/`

8. Создаем виртуальное окружение в папку `~/venv` и ставим зависимости

`virtualenv -p python3 venv`

`source ./venv/bin/activate`

`pip3 install -r ./flowers/requirements.txt`

9. Создаем папку для `www` сокета и логов, чтобы туда мог писать пользователь `www-data`

`mkdir /home/ra/www`
`chown -R /home/ra/www ra:www-data` 

(еще делал `usermod -a -G ra www-data` и `usermod -a -G www-data ra`, но наверное это лишнее)

10. Линкуем конфигурацию `nginx`

`sudo ln -s /home/ra/conf/flowers.conf /etc/nginx/site-enabled`

10. Перезапускаем `nginx` 

`sudo systemctl restart nginx.service`

запускаем `uwsgi`

`sudo systemctl start emperor.uwsgi.service`

11. проверяем статусы служб и проверяем лог, все ли нормально

`sudo systemctl status emperor.uwsgi.service`

`sudo systemctl status nginx.uwsgi.service`

`cat /home/ra/www/uwsgi.log`

12. Если все норм, то включаем службу
`sudo systemctl enable emperor.uwsgi.service`