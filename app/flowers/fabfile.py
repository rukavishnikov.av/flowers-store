from fabric.api import *
from fabric.contrib.files import exists
from fabric.contrib.project import rsync_project

# app name
USERNAME = 'ra'
APP_NAME = 'flowers'
# deployment path
DEPLOY_PATH = '/home/' + USERNAME + '/' + APP_NAME  #/home/ra/flowers
DEPLOY_PATH_FULL = DEPLOY_PATH+'/'+APP_NAME  #/home/ra/flowers/flowers

# host for deployment
env.hosts = ['188.226.179.245']
# deploying user
env.user = 'ra'

def deploy():
    rsync()
    #migrate()


# def migrate():
#     #activate virtualenv and migrate db
#     run('. ' + DEPLOY_PATH + '/venv/bin/activate')
#     run('python ' + DEPLOY_PATH_FULL + '/manage.py makemigrations')
#     run('python ' + DEPLOY_PATH_FULL + '/manage.py migrate')
#     run('python ' + DEPLOY_PATH_FULL + '/manage.py collectstatic')

def rsync():
    # make dir app
    if not exists(DEPLOY_PATH):
        sudo('mkdir ' + DEPLOY_PATH)

    sudo('chown -R %s %s' % (env.user, DEPLOY_PATH))

    #copy to server
    rsync_project(remote_dir=DEPLOY_PATH + '/', extra_opts='-O',
                  local_dir='./', delete=True,
                  exclude=['*.pyc',
                           '.*',
                           '.*/',
                           'fabfile.py',
                           '__pycache__/',
                           ])