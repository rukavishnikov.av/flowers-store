from django.contrib import admin
from .models import *

class OrderMessageAdmin(admin.ModelAdmin):
    list_display = [field.name for field in OrderMessage._meta.fields]
    search_fields = ['name', 'subject', 'message', 'phone']
    class Meta:
        model = OrderMessage


# Register your models here.
admin.site.register(OrderMessage, OrderMessageAdmin)
