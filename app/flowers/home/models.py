from django.db import models

# Messages from site to me
class OrderMessage(models.Model):
    name = models.CharField(max_length=128)
    email = models.EmailField()
    phone = models.CharField(max_length=128)
    subject = models.CharField(max_length=128)
    message = models.TextField()
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False,auto_now=True)

    def __str__(self):
        return '%s %s Created (%s)' % (self.name, self.message, self.created)