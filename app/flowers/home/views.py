from django.shortcuts import render,HttpResponseRedirect
#from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.conf import settings

from .forms import OrderMessageForm

# views
def home(request):
    return render(request, 'home/home.html', locals())

def index(request):

    form = OrderMessageForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():

        # get data from request
        data=form.cleaned_data
        new_form = form.save()

        #send email
        message_id = 'пока нет'
        subject = data['subject']
        contact_phone = data['phone']
        contact_email = data['email']
        name = data['name']
        message = data['message']
        my_email = getattr(settings, "EMAIL_HOST_USER", None)

        text_content = '''
            Номер заказа: %(msg_id)s\n
            Имя отправителя: %(name)s\n
            Телефон: %(contact_phone)s\n
            Email: %(contact_email)s\n
            \n
            Сообщение:\n
            %(message)s
            '''%{"msg_id":message_id,
                 "name":name,
                 "contact_phone":contact_phone,
                 "contact_email":contact_email,
                 "message":message
                }

        html_content = '''
                        <strong>Номер заказа</strong> %(msg_id)s <br>
                        <strong>Имя отправителя</strong> %(name)s <br>
                        <strong>Телефон</strong> %(contact_phone)s <br>
                        <strong>Email</strong> %(contact_email)s <br><br>
                        <strong>Сообщение</strong> <br>
                        %(message)s
                    '''%{"msg_id":message_id,
                         "name":name,
                         "contact_phone":contact_phone,
                         "contact_email":contact_email,
                         "message":message
                        }

        msg = EmailMultiAlternatives(subject, text_content, my_email, [my_email])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

        return HttpResponseRedirect("/")
    else:
        form = OrderMessageForm(request.POST or None)
    return render(request, 'home/index.html', locals())
