from django.contrib import admin
from .models import *

class ProductImageInline(admin.TabularInline):
    model = ProductImage

class ProductAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Product._meta.fields]
    inlines = [ProductImageInline]
    extra = 0

class ProductImageAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ProductImage._meta.fields]


# Registered models
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductImage, ProductImageAdmin)