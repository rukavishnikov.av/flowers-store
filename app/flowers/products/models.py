from django.db import models

#Model about products
class Product(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True, default=None)
    description = models.TextField(blank=True, null=True, default=None)
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False,auto_now=True)

    def __str__(self):
        return '%s' % self.name

#Model for images of products
class ProductImage(models.Model):
    product = models.ForeignKey(Product, blank=True, null=True, default=None)
    image_fullsize = models.ImageField(upload_to='products_images/fullsize', blank=True, null=True, default=None) #free size, but not big size
    image_thumbnails = models.ImageField(upload_to='products_images/thumbnails', blank=True, null=True, default=None) #650 × 350 pixels
    is_active =models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False,auto_now=True)

    def __str__(self):
        return '%s' % self.product.name
